package com.chordsheets;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.chordsheets.entities.Chord;
import com.chordsheets.entities.ChordSheet;
import com.chordsheets.entities.ChordType;
import com.chordsheets.entities.ChordVariation;
import com.chordsheets.entities.Tuning;
import com.chordsheets.entities.tunings.Guitar6StringTuningTable;
import com.chordsheets.tools.DefaultMusicGlobalStateManager;

public class ChordSheetsActivity extends Activity implements OnItemClickListener {

	private ListView mChordSheetsListView;
	private List<ChordSheet> mChordSheetsList = new ArrayList<ChordSheet>();
	private ChordSheetsListViewAdapter mChordSheetsListViewAdapter;
	private DefaultMusicGlobalStateManager mMusicGlobalStateManager;
	private List<ChordType> mAvailableChordTypes;

	private GridView mChordVariationGridView;
	private ChordsLibrarySoundManager mChordsLibraryService;

	private ChordVariationListAdapter mVariationListAdapter;
	private ChordSheet mCurrentChordSheet;

	private LinearLayout mChordSheetsLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		mChordsLibraryService = new ChordsLibrarySoundManager(this);
		mMusicGlobalStateManager = new DefaultMusicGlobalStateManager(getSharedPreferences("", 0));
		mAvailableChordTypes = ChordType.getAvailableChordTypes();

		setContentView(R.layout.chord_sheets_layout);
		mChordSheetsLayout = (LinearLayout) findViewById(R.id.chord_sheets_layout);
		createChordSheetsListView();
		createChordVariationGridView();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			mChordSheetsLayout.setOrientation(LinearLayout.HORIZONTAL);
			LinearLayout.LayoutParams lpGridHorizontal = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT);
			lpGridHorizontal.weight = 2;
			mChordVariationGridView.setLayoutParams(lpGridHorizontal);
			LinearLayout.LayoutParams lpListHorizontal = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT);
			lpListHorizontal.weight = 1;
			mChordSheetsListView.setLayoutParams(lpListHorizontal);
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			mChordSheetsLayout.setOrientation(LinearLayout.VERTICAL);
			LinearLayout.LayoutParams lpGridHorizontal = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
			lpGridHorizontal.weight = 2;
			mChordVariationGridView.setLayoutParams(lpGridHorizontal);
			LinearLayout.LayoutParams lpListHorizontal = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0);
			lpListHorizontal.weight = 1;
			mChordSheetsListView.setLayoutParams(lpListHorizontal);
		}
	}

	private void setColumnGridView() {
		Resources res = getResources();
		int[] column = res.getIntArray(R.array.column_chord_variation_grid_view);
		mChordVariationGridView.setNumColumns(column[0]);
	}

	private void createChordVariationGridView() {
		mChordVariationGridView = (GridView) findViewById(R.id.chord_sheet_grid_view);
		mVariationListAdapter = new ChordVariationListAdapter(this, 0);
		mChordVariationGridView.setAdapter(mVariationListAdapter);
		mChordVariationGridView.setOnItemClickListener(this);
		
		setColumnGridView();
	}

	private void createChordSheetsListView() {
		mChordSheetsListView = (ListView) findViewById(R.id.chord_sheets_list_view);
		mChordSheetsList.add(fillListChordVariationGamma0("C_Gamma"));
		mChordSheetsList.add(fillListChordVariationGamma1("Cm_Gamma"));
		mChordSheetsList.add(fillListChordVariationGamma2("D_Gamma"));
		mChordSheetsList.add(fillListChordVariationGamma3("Gamma"));
		mChordSheetsListViewAdapter = new ChordSheetsListViewAdapter(this, 0, mChordSheetsList);
		mChordSheetsListView.setAdapter(mChordSheetsListViewAdapter);
		mChordSheetsListView.setOnItemClickListener(this);
	}

	private ChordVariation createChordVariation(int chordTypeIndex, int rootNoteIndex, int variationGroupIndex,
			int variationIndex) {
		Tuning tuning = new Guitar6StringTuningTable().getTuningTable().get(0);
		ChordType type = mAvailableChordTypes.get(chordTypeIndex);
		Chord chord = Chord.createChord(type, rootNoteIndex, tuning);
		ChordVariation variation = chord.getVariationGroupForIndex(variationGroupIndex).get(variationIndex);
		return variation;
	}

	private ChordSheet fillListChordVariationGamma0(String nameChordSheet) {
		List<ChordVariation> listVariation = new ArrayList<ChordVariation>();
		listVariation.add(createChordVariation(0, 0, 0, 0));
		listVariation.add(createChordVariation(0, 2, 0, 0));
		listVariation.add(createChordVariation(0, 4, 0, 0));
		listVariation.add(createChordVariation(0, 5, 0, 0));
		listVariation.add(createChordVariation(0, 7, 0, 0));
		listVariation.add(createChordVariation(0, 9, 0, 0));
		listVariation.add(createChordVariation(0, 11, 0, 0));
		ChordSheet chordSheet = new ChordSheet(listVariation, nameChordSheet);
		return chordSheet;
	}

	private ChordSheet fillListChordVariationGamma1(String nameChordSheet) {
		List<ChordVariation> listVariation = new ArrayList<ChordVariation>();
		listVariation.add(createChordVariation(1, 0, 0, 0));
		listVariation.add(createChordVariation(1, 2, 0, 0));
		listVariation.add(createChordVariation(1, 4, 0, 0));
		listVariation.add(createChordVariation(1, 5, 0, 0));
		listVariation.add(createChordVariation(1, 7, 0, 0));
		listVariation.add(createChordVariation(1, 9, 0, 0));
		listVariation.add(createChordVariation(1, 11, 0, 0));
		ChordSheet chordSheet = new ChordSheet(listVariation, nameChordSheet);
		return chordSheet;
	}

	private ChordSheet fillListChordVariationGamma2(String nameChordSheet) {
		List<ChordVariation> listVariation = new ArrayList<ChordVariation>();
		listVariation.add(createChordVariation(1, 0, 0, 0));
		listVariation.add(createChordVariation(0, 2, 0, 0));
		listVariation.add(createChordVariation(1, 4, 0, 0));
		listVariation.add(createChordVariation(1, 5, 0, 0));
		listVariation.add(createChordVariation(0, 7, 0, 0));
		listVariation.add(createChordVariation(0, 9, 0, 0));
		listVariation.add(createChordVariation(2, 11, 0, 0));
		ChordSheet chordSheet = new ChordSheet(listVariation, nameChordSheet);
		return chordSheet;
	}

	private ChordSheet fillListChordVariationGamma3(String nameChordSheet) {
		List<ChordVariation> listVariation = new ArrayList<ChordVariation>();
		listVariation.add(createChordVariation(1, 0, 0, 0));
		listVariation.add(createChordVariation(1, 3, 0, 0));
		listVariation.add(createChordVariation(1, 5, 0, 0));
		listVariation.add(createChordVariation(1, 6, 0, 0));
		listVariation.add(createChordVariation(5, 7, 0, 0));
		listVariation.add(createChordVariation(1, 10, 0, 0));
		listVariation.add(createChordVariation(1, 0, 0, 0));
		ChordSheet chordSheet = new ChordSheet(listVariation, nameChordSheet);
		return chordSheet;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		if (parent.getId() == mChordSheetsListView.getId()) {
			ChordSheetsListViewAdapter adapter = (ChordSheetsListViewAdapter) parent.getAdapter();
			mCurrentChordSheet = adapter.getItem(position);
			mVariationListAdapter.refreshList((ArrayList<ChordVariation>) mCurrentChordSheet.getChordVariationList());
		}
		if (parent.getId() == mChordVariationGridView.getId()) {
			ChordView item = null;
			item = (ChordView) view;
			if (item != null) {
				Animation scaleStart = AnimationUtils.loadAnimation(this, R.anim.chord_scale_start);
				Animation scaleNext = AnimationUtils.loadAnimation(this, R.anim.chord_scale);
				//view.startAnimation(scaleStart);
				view.startAnimation(scaleNext);
				mChordsLibraryService.playChord(item.getChordVariation());
			}
		}
	}
}
