package com.chordsheets;

import com.chordsheets.entities.ChordVariation;

interface IChordsLibraryController {

	public void playChord(ChordVariation variation);

}
