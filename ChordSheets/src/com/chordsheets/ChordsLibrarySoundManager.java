package com.chordsheets;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import com.chordsheets.entities.ChordVariation;
import com.chordsheets.entities.Note;
import com.chordsheets.entities.temperament.Temperament.TemperamentTypes;
import com.ultimateguitar.tools.notation.MusicFacade;

public final class ChordsLibrarySoundManager  {

	public static final int SERVICE_ID = R.id.service_chords;

	protected final SoundPool mSoundPool;
	protected final List<Integer> mSoundIds;
	protected final List<Note> mFrequencies;
	private Context mContext;
	protected static final float[] sBaseFrequencies = new float[] { 329.6f, 246.9f, 196.0f, 146.8f, 110.0f, 82.4f,
			659.2f, 493.9f, 392.0f, 293.7f, 220.0f, 164.8f };

	public ChordsLibrarySoundManager(Context context) {
		
		mFrequencies = MusicFacade.generateNoteSystem(TemperamentTypes.EQUAL_12, MusicFacade.BASENOTE).getListNotes();
		mSoundPool = new SoundPool(12, AudioManager.STREAM_MUSIC, 0);
		mSoundIds = new ArrayList<Integer>();
		mContext = context;
		prepareModel();
	}


	public void prepareModel() {
		int[] soundsData = { R.raw.chlib_e4, R.raw.chlib_b3, R.raw.chlib_g3, R.raw.chlib_d3, R.raw.chlib_a2,
				R.raw.chlib_e2, R.raw.chlib_e5, R.raw.chlib_b4, R.raw.chlib_g4, R.raw.chlib_d4, R.raw.chlib_a3,
				R.raw.chlib_e3 };
		int length = soundsData.length;
		for (int i = 0; i < length; i++) {
			mSoundIds.add(mSoundPool.load(mContext, soundsData[i], 1));
		}
	}


	public void releaseModel() {
		for (Integer soundId : mSoundIds) {
			mSoundPool.unload(soundId);
		}
		mSoundIds.clear();
	}

	public void playSound(int index, ChordVariation variation) {
		int[] playNotes = variation.getNoteFullIndexes();
		playSound(playNotes[index]);
	}

	public void playSound(int noteFullIndex) {
		float noteFrequency = mFrequencies.get(noteFullIndex).frequency;
		int soundIndex = -1;
		int soundSize = sBaseFrequencies.length;
		float frequencyRelation = Integer.MAX_VALUE;
		for (int i = 0; i < soundSize; i++) {
			float baseFrequency = sBaseFrequencies[i];
			float relation = noteFrequency / baseFrequency;
			if (Math.abs(relation - 1) < Math.abs(frequencyRelation-1)) {
				frequencyRelation = relation;
				soundIndex = i;
			}
		}
		mSoundPool.play(mSoundIds.get(soundIndex), 0.99f, 0.99f, 1, 0, frequencyRelation);
	}

	public void playChord(final int[] playNotes) {
		Timer timer = null;
		timer = new Timer();
		playEmptySound();
		timer.schedule(new ChordTimerTask(playNotes), 0, 60);
	}

	public void playChord(ChordVariation variation) {
		final int[] playNotes = variation.getNoteFullIndexes();
		playChord(playNotes);
	}
	

	private void playEmptySound() {
		Integer index = mSoundIds.get(0);
		mSoundPool.play(index, 0f, 0f, 1, 0, 1.0f);
	}
	
	private class ChordTimerTask extends TimerTask{

		private int i;
		private final int[] mPlayNotes;
		public ChordTimerTask(final int[] playNotes){
			mPlayNotes = playNotes.clone();
			i = mPlayNotes.length - 1;
		}
		@Override
		public void run() {
			if (i >= 0) {
				if (mPlayNotes[i] >= 0) {
					playSound(mPlayNotes[i]);
				}
			}
			i--;
			if (i == -1) {
				this.cancel();
			}
		}
		
	}

}
