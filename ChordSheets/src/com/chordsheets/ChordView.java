package com.chordsheets;

import java.util.List;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.util.AttributeSet;
import android.view.View;

import com.chordsheets.entities.Barre;
import com.chordsheets.entities.ChordVariation;
import com.chordsheets.entities.Note.NamingConvention;

public class ChordView extends View {

	private static final float STRING_COUNT = 6;
	private static final float CELL_SIZE_MULTIPLYER = 1.4f;
	private static final float FRET_COUNT = 4;
	private final Paint mFingerPaint;
	private final Paint mStringLinePaint;
	private final Paint mClosedStringLinePaint;
	private final Paint mFretLinePaint;
	private final Paint mEdgeFretLinePaint;
	private final Paint mFretTextPaint;
	private final Paint mActiveMarkerPaint;
	private final Paint mChordNameTextPaint;

	private float mViewMargin;
	private float mFingerSize;
	private float mBarreWidth;
	private float mFingerMinimalMargin;
	private float mActiveMarkerSize;
	private float mActiveMarkerMargin;
	private float mChordNameTextMargin;
	private float mFretTextMargin;

	private ChordVariation chordVariation;

	private float mFretStepHeight;
	private float mStringStepWidth;
	private float mStartStringX;
	private float mStartFretY;

	public ChordView(Context context) {
		this(context, null);
	}

	public ChordView(Context context, AttributeSet attrs) {
		this(context, attrs, R.attr.chordVariationViewStyle);
	}

	public ChordView(Context context, AttributeSet attrs, int defStyleAttr) {
		this(context, attrs, defStyleAttr, R.style.Widget_ChordVariationView);
	}

	public ChordView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr);
		TypedArray styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.ChordVariationView,
				defStyleAttr, defStyleRes);
		try {
			mFingerPaint = createFillPaint(styledAttributes, R.styleable.ChordVariationView_fingerColor);
			mStringLinePaint = createLinePaint(styledAttributes, R.styleable.ChordVariationView_stringLineColor,
					R.styleable.ChordVariationView_stringLineWidth);
			mClosedStringLinePaint = createLinePaint(styledAttributes,
					R.styleable.ChordVariationView_closedeStringLineColor,
					R.styleable.ChordVariationView_stringLineWidth);
			mFretLinePaint = createLinePaint(styledAttributes, R.styleable.ChordVariationView_fretLineColor,
					R.styleable.ChordVariationView_fretLineWidth);
			mEdgeFretLinePaint = createLinePaint(styledAttributes, R.styleable.ChordVariationView_fretLineColor,
					R.styleable.ChordVariationView_edgeFretLineWidth);
			mFretTextPaint = createTextPaint(styledAttributes, R.styleable.ChordVariationView_fretTextColor,
					R.styleable.ChordVariationView_fretTextSize);
			mActiveMarkerPaint = createLinePaint(styledAttributes, R.styleable.ChordVariationView_activeMarkerColor,
					R.styleable.ChordVariationView_activeMarkerLineWidth);
			mChordNameTextPaint = createTextPaint(styledAttributes, R.styleable.ChordVariationView_chordNameTextColor,
					R.styleable.ChordVariationView_chordNameTextSize);

			mFingerSize = styledAttributes.getDimensionPixelSize(R.styleable.ChordVariationView_fingerSize, 0);
			mBarreWidth = styledAttributes.getDimension(R.styleable.ChordVariationView_barreWidth, 0);
			mFingerMinimalMargin = styledAttributes.getDimensionPixelSize(
					R.styleable.ChordVariationView_fingerMinimalMargin, 0);
			mActiveMarkerSize = styledAttributes.getDimensionPixelSize(R.styleable.ChordVariationView_activeMarkerSize,
					0);
			mActiveMarkerMargin = styledAttributes.getDimension(R.styleable.ChordVariationView_activeMarkerMargin, 0);
			mChordNameTextMargin = styledAttributes.getDimension(R.styleable.ChordVariationView_chordNameTextMargin, 0);
			mFretTextMargin = styledAttributes.getDimension(R.styleable.ChordVariationView_fretTextMargin, 0);
			mViewMargin = styledAttributes.getDimension(R.styleable.ChordVariationView_margin, 0);

		} finally {
			styledAttributes.recycle();
		}
	}

	public ChordVariation getChordVariation() {
		return chordVariation;
	}

	public void setChordVariation(ChordVariation chordVariation) {
		this.chordVariation = chordVariation;
	}

	private static Paint createTextPaint(TypedArray styledAttributes, int resourceStyleColor, int resourceStyleTextSize) {
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(styledAttributes.getColor(resourceStyleColor, 0));
		paint.setTextSize(styledAttributes.getDimension(resourceStyleTextSize, 0));
		paint.setTextAlign(Paint.Align.CENTER);
		return paint;
	}

	private static Paint createFillPaint(TypedArray styledAttributes, int resourceStyleColor) {
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(styledAttributes.getColor(resourceStyleColor, 0));
		return paint;
	}

	private static Paint createLinePaint(TypedArray styledAttributes, int resourceStyleColor, int resourceStyleWidth) {
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(styledAttributes.getColor(resourceStyleColor, 0));
		paint.setStrokeWidth(styledAttributes.getDimension(resourceStyleWidth, 0));
		paint.setStyle(Paint.Style.STROKE);
		return paint;
	}

	public int getFingerColor() {
		return mFingerPaint.getColor();
	}

	public void setFingerColor(int color) {
		mFingerPaint.setColor(color);
		invalidate();
	}

	public float getFingerSize() {
		return mFingerSize;
	}

	public void setFingerSize(float fingerSize) {
		mFingerSize = fingerSize;
		requestLayout();
		invalidate();
	}

	public float getBarreWidth() {
		return mBarreWidth;
	}

	public void setBarreWidth(float barreWidth) {
		mBarreWidth = barreWidth;
		requestLayout();
		invalidate();
	}

	public float getFingerMinimalVerticalMargin() {
		return mFingerMinimalMargin;
	}

	public void setFingerMinimalVerticalMargin(float margin) {
		mFingerMinimalMargin = margin;
		requestLayout();
		invalidate();
	}

	public int getStringLineColor() {
		return mStringLinePaint.getColor();
	}

	public void setStringLineColor(int color) {
		mStringLinePaint.setColor(color);
		invalidate();
	}

	public float getStringLineWidth() {
		return mStringLinePaint.getStrokeWidth();
	}

	public void setStringLineWidth(float lineWidth) {
		mStringLinePaint.setStrokeWidth(lineWidth);
		requestLayout();
		invalidate();
	}

	public int getFretLineColor() {
		return mFretLinePaint.getColor();
	}

	public void setFretLineColor(int color) {
		mFretLinePaint.setColor(color);
		mEdgeFretLinePaint.setColor(color);
		invalidate();
	}

	public float getFretLineWidth() {
		return mFretLinePaint.getStrokeWidth();
	}

	public void setFretLineWidth(float lineWidth) {
		mFretLinePaint.setStrokeWidth(lineWidth);
		requestLayout();
		invalidate();
	}

	public float getEdgeFretLineWidth() {
		return mEdgeFretLinePaint.getStrokeWidth();
	}

	public void setEdgeFretLineWidth(float lineWidth) {
		mEdgeFretLinePaint.setStrokeWidth(lineWidth);
		requestLayout();
		invalidate();
	}

	public int getFretTextColor() {
		return mFretTextPaint.getColor();
	}

	public void setFretTextColor(int color) {
		mFretTextPaint.setColor(color);
		invalidate();
	}

	public float getFretTextSize() {
		return mFretTextPaint.getTextSize();
	}

	public void setFretTextSize(float textSize) {
		mFretTextPaint.setTextSize(textSize);
		requestLayout();
		invalidate();
	}

	public float getChordNameTextSize() {
		return mChordNameTextPaint.getTextSize();
	}

	public void setChordNameTextSize(float textSize) {
		mChordNameTextPaint.setTextSize(textSize);
		requestLayout();
		invalidate();
	}

	public int getActiveMarkerColor() {
		return mActiveMarkerPaint.getColor();
	}

	public void setActiveMarkerColor(int color) {
		mActiveMarkerPaint.setColor(color);
		invalidate();
	}

	public float getActiveMarkerSize() {
		return mActiveMarkerSize;
	}

	public void setActiveMarkerSize(float activeMarkerSize) {
		mActiveMarkerSize = activeMarkerSize;
		requestLayout();
		invalidate();
	}

	public float getActiveMarkerLineWidth() {
		return mActiveMarkerPaint.getStrokeWidth();
	}

	public void setActiveMarkerLineWidth(float lineWidth) {
		mActiveMarkerPaint.setStrokeWidth(lineWidth);
		/*
		 * В отличии от других методов, меняющих краскам strokeWidth, здесь не нужно вызывать requestLayout(), потому
		 * что изменение толщины линии маркера не влияет на размеры вьюхи.
		 */
		invalidate();
	}

	public float getActiveMarkerMargin() {
		return mActiveMarkerMargin;
	}

	public void setActiveMarkerMargin(float activeMarkerMargin) {
		mActiveMarkerMargin = activeMarkerMargin;
		requestLayout();
		invalidate();
	}

	public float getFretTextMargin() {
		return mFretTextMargin;
	}

	public void setFretTextMargin(float fretTextMargin) {
		mFretTextMargin = fretTextMargin;
		requestLayout();
		invalidate();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		float desiredWidth = getPaddingLeft() + getPaddingRight() + getDesiredWidthWithoutPaddings();
		float desiredHeight = getPaddingTop() + getPaddingBottom() + getDesiredHeightWithoutPaddings();
		int widthSize = (int) Math.max(desiredWidth, MeasureSpec.getSize(widthMeasureSpec));
		int heightSize = (int) Math.max(desiredHeight, MeasureSpec.getSize(heightMeasureSpec));
		heightSize = (int) Math.max(widthSize, heightSize);
		setMeasuredDimension(widthSize, heightSize);
	}

	private float getDesiredHeightWithoutPaddings() {
		float height = getDesiredActiveMarkerHeight() + getDesiredStringFretNetHeight()
				+ getDesiredChordNameTextHeight();
		return height;
	}

	private float getDesiredActiveMarkerHeight() {
		return getActiveMarkerSize() + 2 * getActiveMarkerMargin();
	}

	private float getDesiredStringFretNetHeight() {
		float desiredDiametr = mFingerSize;
		float width = (desiredDiametr * CELL_SIZE_MULTIPLYER + getFretLineWidth()) * FRET_COUNT + getFretLineWidth();
		return width;
	}

	private float getDesiredChordNameTextHeight() {
		FontMetrics fm = mChordNameTextPaint.getFontMetrics();
		float desiredTextHeight = fm.bottom + fm.top + mChordNameTextPaint.getTextSize() + mChordNameTextMargin * 2;
		return desiredTextHeight;
	}

	private float getDesiredWidthWithoutPaddings() {
		float width = 2 * mFingerMinimalMargin + mFingerSize + getDesiredStringFretNetWidth()
				+ getDesiredFretTextWidth();
		return width;
	}

	private float getDesiredFretTextWidth() {
		Paint paint = new Paint();
		paint.setTextSize(getFretTextSize());
		return paint.measureText(Integer.toString(chordVariation.fret)) + getFretTextMargin();
	}

	private float getDesiredStringFretNetWidth() {
		float desiredDiametr = mFingerSize;
		float width = (desiredDiametr * CELL_SIZE_MULTIPLYER) * (STRING_COUNT - 1) + getStringLineWidth();
		return width;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.translate(getPaddingLeft(), getPaddingTop());
		float width = getWidth() - getPaddingLeft() - getPaddingRight();
		float height = getHeight() - getPaddingTop() - getPaddingBottom();
		canvas.clipRect(0, 0, width, height);
		doDraw(canvas, width, height);
		canvas.restore();
	}

	private void doDraw(Canvas canvas, float width, float height) {
		float leftTopX = mFingerSize / 2;
		float leftTopY = getDesiredActiveMarkerHeight();
		float rightBottomX = width - mFingerMinimalMargin - leftTopX - getDesiredFretTextWidth();
		float rightBottomY = height - getDesiredChordNameTextHeight() - leftTopY;
		canvas.save();
		canvas.clipRect(leftTopX, leftTopY, rightBottomX, rightBottomY);
		drawFrets(canvas, leftTopX, leftTopY, rightBottomX, rightBottomY);
		drawStrings(canvas, leftTopX, leftTopY, rightBottomX, rightBottomY);
		canvas.restore();
		drawChordName(canvas, width, height);
		drawFretNumber(canvas, width, height);
		drawFinger(canvas, width, height);
		drawBarre(canvas, width, height);
		drawActiveMarkers(canvas, leftTopX, leftTopY, rightBottomX, rightBottomY);
	}

	private void drawBarre(Canvas canvas, float width, float height) {
		List<Barre> listBarre = chordVariation.getBarres();
		Paint barrePaint = mFingerPaint;
		barrePaint.setStrokeWidth(mFingerSize);
		for (int i = 0; i < listBarre.size(); i++) {
			float centrXStart = (STRING_COUNT - listBarre.get(i).startString - 1) * mStringStepWidth + mStartStringX;
			float centrXLast = (STRING_COUNT - listBarre.get(i).lastString - 1) * mStringStepWidth + mStartStringX;
			float centrY = (listBarre.get(i).fret - chordVariation.fret + 0.5f) * mFretStepHeight + mStartFretY;
			canvas.drawLine(centrXStart, centrY, centrXLast, centrY, mFingerPaint);
		}
	}

	private void drawFinger(Canvas canvas, float width, float height) {
		int fretLengtht = chordVariation.getFrets().length;
		int[] fretArray = chordVariation.getFrets().clone();
		for (int stringNumber = 0; stringNumber < fretLengtht; stringNumber++) {
			if (fretArray[stringNumber] > 0) {
				setFinger(canvas, width, height, stringNumber, fretArray[stringNumber]);
			}
		}
	}

	private void setFinger(Canvas canvas, float width, float height, int stringNumber, int fret) {
		float centrX = (STRING_COUNT - stringNumber - 1) * mStringStepWidth + mStartStringX;
		float centrY = (fret - chordVariation.fret + 0.5f) * mFretStepHeight + mStartFretY;
		canvas.drawCircle(centrX, centrY, mFingerSize / 2, mFingerPaint);
	}

	private void drawChordName(Canvas canvas, float width, float height) {
		float positionX = 1.5f * (width) / (STRING_COUNT - 1);
		canvas.drawText(
				chordVariation.getName(getContext(), NamingConvention.ENGLISH_NAMING_CONVENTION_WITH_SPECIAL_SYMBOLS),
				positionX, height - getDesiredChordNameTextHeight() + mChordNameTextMargin, mChordNameTextPaint);
	}

	private void drawFretNumber(Canvas canvas, float width, float height) {
		float desiredFretTextWidth = getDesiredFretTextWidth() - getFretTextMargin();
		float positionY = mFretStepHeight + height / FRET_COUNT / 3;
		canvas.drawText(String.valueOf(chordVariation.fret), width - desiredFretTextWidth, positionY, mFretTextPaint);
	}

	private void drawFrets(Canvas canvas, float leftTopX, float leftTopY, float rightBottomX, float rightBottomY) {
		float startX = leftTopX;
		mStartFretY = leftTopY + getFretLineWidth() / 2;
		float startY = mStartFretY;
		float stopX = rightBottomX;
		float stopY = mStartFretY;
		mFretStepHeight = (rightBottomY - leftTopY) / (FRET_COUNT);
		for (int i = 0; i < FRET_COUNT; i++) {
			if ((i == 0) && (chordVariation.fret == 1)) {
				canvas.drawLine(startX, startY, stopX, stopY, mEdgeFretLinePaint);
			} else {
				canvas.drawLine(startX, startY, stopX, stopY, mFretLinePaint);
			}
			startY += mFretStepHeight;
			stopY = startY;
		}
	}

	private void drawStrings(Canvas canvas, float leftTopX, float leftTopY, float rightBottomX, float rightBottomY) {
		mStartStringX = leftTopX + getStringLineWidth() / 2;
		float startX = mStartStringX;
		float startY = leftTopY;
		float stopX = mStartStringX;
		float stopY = rightBottomY;
		mStringStepWidth = (rightBottomX - mStartStringX - getStringLineWidth() / 2) / (STRING_COUNT - 1);
		for (int i = 0; i < STRING_COUNT; i++) {
			if (chordVariation.getFrets()[(int) (STRING_COUNT - i - 1)] == -1) {
				canvas.drawLine(startX, startY, stopX, stopY, mClosedStringLinePaint);
			} else {
				canvas.drawLine(startX, startY, stopX, stopY, mStringLinePaint);
			}
			startX += mStringStepWidth;
			stopX = startX;
		}
	}

	private void drawActiveMarkers(Canvas canvas, float leftTopX, float leftTopY, float rightBottomX, float rightBottomY) {
		float startX = mStartStringX;
		float startY = getActiveMarkerSize();
		int[] fretArray = chordVariation.getFrets().clone();
		for (int i = 0; i < STRING_COUNT; i++) {
			if (chordVariation.getFrets()[(int) (STRING_COUNT - i - 1)] == -1) {
				drawClosedStringActiveMarker(canvas, startX, startY);
			}
			if (fretArray[(int) (STRING_COUNT - i - 1)] == 0) {
				drawOpenStringActiveMarker(canvas, startX, startY);
			}
			startX += mStringStepWidth;
		}
	}

	private void drawClosedStringActiveMarker(Canvas canvas, float startX, float startY) {
		float d = getActiveMarkerSize() / 2;
		canvas.drawLine(startX - d, startY - d, startX + d, startY + d, mActiveMarkerPaint);
		canvas.drawLine(startX - d, startY + d, startX + d, startY - d, mActiveMarkerPaint);
	}

	private void drawOpenStringActiveMarker(Canvas canvas, float startX, float startY) {
		float r = getActiveMarkerSize() / 2;
		canvas.drawCircle(startX, startY, r, mActiveMarkerPaint);
	}

}
