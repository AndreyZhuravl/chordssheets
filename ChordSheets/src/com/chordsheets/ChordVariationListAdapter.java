package com.chordsheets;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.chordsheets.entities.ChordVariation;

public class ChordVariationListAdapter extends ArrayAdapter<ChordVariation>{

	private List<ChordVariation> mChordVariationList;
	private int mPadding;

	public ChordVariationListAdapter(Context context, int resource) {
		super(context, resource);
		mChordVariationList = new ArrayList<ChordVariation>();
		mPadding = (int) context.getResources().getDimension(R.dimen.chord_variation_margin);
	}

	public void refreshList(ArrayList<ChordVariation> progressionList) {
		mChordVariationList = new ArrayList<ChordVariation>(progressionList);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mChordVariationList.size();
	}

	@Override
	public ChordVariation getItem(int position) {
		ChordVariation cv = null;
		if (mChordVariationList.size() > position) {
			cv = mChordVariationList.get(position);
		}
		return cv;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = null;
		if (convertView == null) {
			item = newView(position, parent);
		} else {
			item = convertView;
		}
		bindView(position, item);
		return item;
	}

	private void bindView(int position, View item) {
		ChordView chordView = (ChordView) item;
		chordView.setChordVariation(mChordVariationList.get(position));
	}

	private View newView(int position, ViewGroup parent) {
		ChordView chordView = new ChordView(getContext());
		chordView.setPadding(mPadding, mPadding, mPadding, mPadding);
		chordView.setChordVariation(mChordVariationList.get(position));
		return chordView;
	}

}
