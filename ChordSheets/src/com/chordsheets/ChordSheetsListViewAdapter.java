package com.chordsheets;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.chordsheets.entities.ChordSheet;

public class ChordSheetsListViewAdapter extends ArrayAdapter<ChordSheet> {

	private List<ChordSheet> mChordSheets;

	public ChordSheetsListViewAdapter(Context context, int resource, List<ChordSheet> chordSheets) {
		super(context, resource);
		mChordSheets = chordSheets;
	}

	@Override
	public int getCount() {
		return mChordSheets.size();
	}
	
	@Override
	public ChordSheet getItem(int position) {
		return mChordSheets.get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;
		if (item == null) {
			item = newView(position, parent);
		}
		bindView(position, item);
		return item;
	}
	
	private void bindView(int position, View item) {
		ViewHolder holder = (ViewHolder) item.getTag();
		TextView textView = holder.nameChordSheet;
		String text = mChordSheets.get(position).getChordSheetName();
		textView.setText(text);		
	}

	private View newView(int position, ViewGroup parent) {
		ViewHolder holder = new ViewHolder();
		View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.chord_sheet_item_view, null);
		holder.nameChordSheet = (TextView) item.findViewById(R.id.chord_sheet_item_name_text_view);
		item.setTag(holder);
		return item;
	}

	private static class ViewHolder{
		public TextView nameChordSheet;
	}

}
