package com.chordsheets.tools;

public class MusicToolsConstants {

	public static final String PREFERENCES_KEY_TUNING = "toolsTuningMode";
	public static final String PREFERENCES_KEY_BASE_NOTE_OCTAVE = "chtBaseNoteOctave";
	public static final String PREFERENCES_KEY_BASE_NOTE_INDEX = "chtBaseNoteIndex";
	public static final String PREFERENCES_KEY_BASE_NOTE_FREQUENCY = "chtBaseNoteFrequency";
	
	public static final int BASE_NOTE_OCTAVE_DEFAULT = 4;
	public static final int BASE_NOTE_INDEX_DEFAULT = 9;
	public static final float BASE_NOTE_FREQUENCY_DEFAULT = 440.0f;
	
	public static final boolean DEFAULT_LEFT_HAND_MODE = false;
}
