package com.chordsheets.tools;

import java.util.ArrayList;
import java.util.List;

import android.content.SharedPreferences;

import com.chordsheets.entities.Note;
import com.chordsheets.entities.Tuning;
import com.chordsheets.entities.tunings.Guitar6StringTuningTable;

public final class DefaultMusicGlobalStateManager implements IMusicGlobalStateManager {

	private final SharedPreferences mApplicationPreferences;
	private final List<StateListener> mStateListeners;
	private final List<Tuning> mTuningsList;
	private int mSelectedTuningIndex;
	private Note mBaseNote;


	public DefaultMusicGlobalStateManager(SharedPreferences preferences) {
		mApplicationPreferences = preferences;
		mStateListeners = new ArrayList<IMusicGlobalStateManager.StateListener>();
		mTuningsList = new Guitar6StringTuningTable().getTuningTable();
		mSelectedTuningIndex = mApplicationPreferences.getInt(MusicToolsConstants.PREFERENCES_KEY_TUNING, 0);
		int baseOctave = mApplicationPreferences.getInt(MusicToolsConstants.PREFERENCES_KEY_BASE_NOTE_OCTAVE,
				MusicToolsConstants.BASE_NOTE_OCTAVE_DEFAULT);
		int baseIndex = mApplicationPreferences.getInt(MusicToolsConstants.PREFERENCES_KEY_BASE_NOTE_INDEX,
				MusicToolsConstants.BASE_NOTE_INDEX_DEFAULT);
		float baseFrequency = mApplicationPreferences.getFloat(MusicToolsConstants.PREFERENCES_KEY_BASE_NOTE_FREQUENCY,
				MusicToolsConstants.BASE_NOTE_FREQUENCY_DEFAULT);
		mBaseNote = new Note(baseOctave, baseIndex, baseFrequency);

	}

	@Override
	public List<Tuning> getTuningsList() {
		return mTuningsList;
	}

	@Override
	public Tuning getSelectedTuning() {
		return mTuningsList.get(mSelectedTuningIndex);
	}

	@Override
	public int getSelectedTuningIndex() {
		return mSelectedTuningIndex;
	}

	@Override
	public void setSelectedTuningIndex(int index) {
		if (mSelectedTuningIndex != index) {
			mSelectedTuningIndex = index;
			SharedPreferences.Editor editor = mApplicationPreferences.edit();
			editor.putInt(MusicToolsConstants.PREFERENCES_KEY_TUNING, index);
			editor.commit();

			for (StateListener listener : mStateListeners) {
				listener.onTuningChanged(this, mSelectedTuningIndex);
			}
		}
	}

	@Override
	public Note getBaseNote() {
		return mBaseNote;
	}

	@Override
	public void setBaseNote(Note baseNote) {
		if (!mBaseNote.equals(baseNote)) {
			mBaseNote = baseNote;
			SharedPreferences.Editor editor = mApplicationPreferences.edit();
			editor.putInt(MusicToolsConstants.PREFERENCES_KEY_BASE_NOTE_OCTAVE, mBaseNote.octave);
			editor.putInt(MusicToolsConstants.PREFERENCES_KEY_BASE_NOTE_INDEX, mBaseNote.index);
			editor.putFloat(MusicToolsConstants.PREFERENCES_KEY_BASE_NOTE_FREQUENCY, mBaseNote.frequency);
			editor.commit();

			for (StateListener listener : mStateListeners) {
				listener.onBaseNoteChanged(this, mBaseNote);
			}
		}
	}

	@Override
	public void registerStateListener(StateListener listener) {
		mStateListeners.add(listener);
	}

	@Override
	public void unregisterStateListener(StateListener listener) {
		mStateListeners.remove(listener);
	}

}
