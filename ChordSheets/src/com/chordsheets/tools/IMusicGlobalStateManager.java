package com.chordsheets.tools;

import java.util.List;

import com.chordsheets.R;
import com.chordsheets.entities.Note;
import com.chordsheets.entities.Tuning;

public interface IMusicGlobalStateManager {

	public static final int MODEL_ID = R.id.music_global_state_manager_id;

	public List<Tuning> getTuningsList();

	public Tuning getSelectedTuning();

	public int getSelectedTuningIndex();

	public void setSelectedTuningIndex(int index);

	public Note getBaseNote();

	public void setBaseNote(Note baseNote);

	public void registerStateListener(StateListener listener);

	public void unregisterStateListener(StateListener listener);

	public static interface StateListener {

		public void onTuningChanged(IMusicGlobalStateManager sender, int selectedTuningIndex);

		public void onBaseNoteChanged(IMusicGlobalStateManager sender, Note baseNote);
	}

}
