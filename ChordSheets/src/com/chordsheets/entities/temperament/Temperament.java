package com.chordsheets.entities.temperament;

import java.util.ArrayList;
import java.util.List;

import com.chordsheets.entities.Note;
import com.chordsheets.entities.NoteSystem;

public abstract class Temperament {

	protected int mBaseOctave;
	protected int mBaseIndex;
	protected double mBaseFrequency;

	public enum TemperamentTypes {
		EQUAL_12, MEANTONE
	}

	protected abstract Note generateNote(int octave, int index);

	public final NoteSystem generateNoteSystem(Note baseNote) {
		List<Note> listNotes = new ArrayList<Note>();
		mBaseOctave = baseNote.octave;
		mBaseIndex = baseNote.index;
		mBaseFrequency = baseNote.frequency;
		int i, j;
		for (i = 0; i < 8; i++) {
			for (j = 0; j < Note.TONES_IN_OCTAVE; j++) {
				listNotes.add(generateNote(i, j));
			}
		}
		return new NoteSystem(listNotes);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(mBaseFrequency);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + mBaseIndex;
		result = prime * result + mBaseOctave;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Temperament other = (Temperament) obj;
		if (Double.doubleToLongBits(mBaseFrequency) != Double.doubleToLongBits(other.mBaseFrequency)) {
			return false;
		}
		if (mBaseIndex != other.mBaseIndex) {
			return false;
		}
		if (mBaseOctave != other.mBaseOctave) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getSimpleName());
		builder.append(" [mBaseOctave=");
		builder.append(mBaseOctave);
		builder.append(", mBaseIndex=");
		builder.append(mBaseIndex);
		builder.append(", mBaseFrequency=");
		builder.append(mBaseFrequency);
		builder.append("]");
		return builder.toString();
	}

}
