package com.chordsheets.entities.temperament;

import com.chordsheets.entities.Note;

public final class EqualTemperament12 extends Temperament {

	public EqualTemperament12() {
	}

	@Override
	public Note generateNote(int octave, int index) {
		float frequency = (float) (mBaseFrequency * Math.pow(2, (1.0 * Note.TONES_IN_OCTAVE * (octave - mBaseOctave)
				+ index - mBaseIndex)
				/ Note.TONES_IN_OCTAVE));
		return new Note(octave, index, frequency);
	}
}
