package com.chordsheets.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import android.content.Context;

public final class Chord {

	/* количество полутонов (ладов) в октаве */
	public static final int FRETS_IN_OCTAVE = 12;
	/* максимально возможное расстояние между ладами в вариации */
	public static final int MAX_FRETS_DISTANCE = 4;
	/* значение лада или звука для неиспользуемой струны */
	public static final int UNUSED_STRING = -1;
	/* значение лада для открытой струны */
	public static final int OPEN_STRING = 0;
	private static final int PERFECT_FIFTH_POSITION_IN_CHORD_SCHEME = 2;
	private static final Set<int[]> sExceptionSchemes;

	private final ChordType mChordType;
	private final int mRootNoteIndex;
	private final List<List<ChordVariation>> mVariationGroups;

	static {
		sExceptionSchemes = new HashSet<int[]>();
		sExceptionSchemes.add(new int[] { 1, 1, 2, 3, 0, 1 });
	}

	public Chord(ChordType chordType, int rootNoteIndex) {
		super();
		mChordType = chordType;
		mRootNoteIndex = rootNoteIndex;
		mVariationGroups = new ArrayList<List<ChordVariation>>();
	}

	public int getRootNoteIndex() {
		return mRootNoteIndex;
	}

	public ChordType getChordType() {
		return mChordType;
	}

	public boolean isEmpty() {
		return mVariationGroups.isEmpty();
	}

	public int getGroupsCount() {
		return mVariationGroups.size();
	}

	public List<ChordVariation> getVariationGroupForIndex(int index) {
		return mVariationGroups.get(index);
	}

	public String getNameForVariationGroup(Context context, int groupIndex, Note.NamingConvention namingConvention) {
		List<ChordVariation> variationGroup = getVariationGroupForIndex(groupIndex);
		ChordVariation anyVariation = variationGroup.get(0);
		return anyVariation.getName(context, namingConvention);
	}

	public String getName(Context context, Note.NamingConvention namingConvention) {
		String rootNoteName = namingConvention.getNoteName(mRootNoteIndex, false);
		String typeName = mChordType.getName(context);
		return String.format(Locale.US, "%s%s", rootNoteName, typeName);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (mVariationGroups.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Chord other = (Chord) obj;
		if (!mVariationGroups.equals(other.mVariationGroups)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getSimpleName());
		builder.append(" [mChordType=");
		builder.append(mChordType);
		builder.append(", mVariationGroups=");
		builder.append(mVariationGroups);
		builder.append("]");
		return builder.toString();
	}

	/**
	 * Фабричный метод для создания аккорда. Здесь должен произвестись перебор возможных вариаций по различным фильтрам,
	 * за исключением фильтра по расстановке пальцев.
	 * 
	 * @param chordType
	 *            - тип аккорда
	 * @param rootNoteIndex
	 *            - индекс ноты
	 * @param tuning
	 *            - Тюнинг
	 * @return
	 */
	public static Chord createChord(ChordType chordType, int rootNoteIndex, Tuning tuning) {
		Chord chord = new Chord(chordType, rootNoteIndex);

		// Шаг 1: заполнение массива и множества используемых нот
		ChordBuildInfo buildInfo = new ChordBuildInfo(chordType, rootNoteIndex, tuning);

		// Шаг 2: подготавливаем множества для вариаций
		int setsCount = (chordType.hasNoBassNotes() ? 1 : chordType.getSchemeSize());
		List<Set<ChordVariation>> listVariationGroups = new ArrayList<Set<ChordVariation>>();
		for (int i = 0; i < setsCount; i++) {
			listVariationGroups.add(new HashSet<ChordVariation>());
		}

		/*
		 * Шаг 3: проходим по всем басовым нотам (т.е. по тем, что входят в схему), проходим по ладам от 0 до
		 * (FRETS_IN_OCTAVE + MAX_FRETS_DISTANCE), по струнам от stringCount-1 до 3(2).
		 */
		int schemeSize = chordType.getSchemeSize();
		int maxFret = (FRETS_IN_OCTAVE + MAX_FRETS_DISTANCE);
		int stringCount = tuning.getNotesCount();
		int param = Math.min(schemeSize - 1, 2);
		for (int schemeNoteCounter = 0; schemeNoteCounter < schemeSize; schemeNoteCounter++) {
			buildInfo.bassNoteIndex = buildInfo.employedNoteIndexes[schemeNoteCounter];
			int setIndex = (chordType.hasNoBassNotes()) ? 0 : schemeNoteCounter;
			Set<ChordVariation> variationGroup = listVariationGroups.get(setIndex);
			for (int fret = 0; fret < maxFret; fret++) {
				for (int currentString = stringCount - 1; currentString > param; currentString--) {
					int openNoteIndex = buildInfo.openStringNoteFullIndexes[currentString];
					int currentNoteIndex = (openNoteIndex + fret) % FRETS_IN_OCTAVE;
					if (currentNoteIndex == buildInfo.bassNoteIndex) {
						int startFret = Math.max(fret - MAX_FRETS_DISTANCE, 0);
						int endFret = fret + 1;
						startFillingVariationSet(variationGroup, buildInfo, currentString, fret, startFret, endFret);
						startFret = Math.max(fret - 1, 0);
						endFret = fret + MAX_FRETS_DISTANCE;
						startFillingVariationSet(variationGroup, buildInfo, currentString, fret, startFret, endFret);
					}
				}
			}
		}

		/*
		 * Шаг 4: из полученных множеств получаем отсортированные массивы
		 */
		for (int i = 0; i < setsCount; i++) {
			Set<ChordVariation> variationGroupAsSet = listVariationGroups.get(i);
			List<ChordVariation> variationGroupAsList = new ArrayList<ChordVariation>(variationGroupAsSet);
			Collections.sort(variationGroupAsList);
			chord.mVariationGroups.add(variationGroupAsList);
		}
		return chord;
	}

	/**
	 * Запуск рекурсивного алгоритма генерации вариации
	 * 
	 * @param variationsSet
	 *            - множество, в которое кладутся сгенерированные вариации.
	 * @param buildInfo
	 *            - набор параметров для генерации вариаций (массивы индексов нот).
	 * @param currentString
	 *            - текущая струна
	 * @param currentFret
	 *            - текущий лад
	 * @param startFret
	 *            - лад, с которого ведется поиск.
	 * @param endFret
	 *            - лад, до которого ведется поиск.
	 */
	private static void startFillingVariationSet(Set<ChordVariation> variationSet, ChordBuildInfo buildInfo,
			int currentString, int currentFret, int startFret, int endFret) {
		buildInfo.usedNoteIndexSet = new HashSet<Integer>();
		buildInfo.usedNoteIndexSet.add(buildInfo.bassNoteIndex);
		int stringCount = buildInfo.openStringNoteFullIndexes.length;
		buildInfo.frets = new int[stringCount];
		Arrays.fill(buildInfo.frets, 0);
		for (int i = currentString + 1; i < stringCount; i++) {
			buildInfo.frets[i] = -1;
		}
		buildInfo.frets[currentString] = currentFret;
		fillVariationSet(variationSet, buildInfo, currentString - 1, currentFret, startFret, endFret, currentFret,
				currentFret);
	}

	private static void fillVariationSet(Set<ChordVariation> variationSet, ChordBuildInfo buildInfo, int currentString,
			int currentFret, int startFret, int endFret, int minFret, int maxFret) {
		if ((maxFret - Math.max(minFret, 1) < MAX_FRETS_DISTANCE) && (minFret < FRETS_IN_OCTAVE)) {
			if (buildInfo.chordType.isFifthAbsencePossible()) {
				int fifthNoteIndex = buildInfo.employedNoteIndexes[PERFECT_FIFTH_POSITION_IN_CHORD_SCHEME];
				buildInfo.usedNoteIndexSet.add(fifthNoteIndex);
			}
			boolean thinnestStringAbsencePossible = buildInfo.chordType.isFifthAbsencePossible();
			if (currentString < 0 || thinnestStringAbsencePossible) {
				if (thinnestStringAbsencePossible && (currentString >= 0)) {
					for (int i = currentString; i >= 0; i--) {
						buildInfo.frets[i] = -1;
					}
				}
				boolean allNotesUsed = buildInfo.employedNoteIndexSet.equals(buildInfo.usedNoteIndexSet);
				if (allNotesUsed && validateBuildInfo(buildInfo)) {
					ChordVariation variation = tryCreateChordVariation(buildInfo, currentString);
					if (variation != null) {
						variationSet.add(variation);
					}
				}
			}
		}
		int schemeSize = buildInfo.employedNoteIndexes.length;
		if (currentString >= 0) {
			for (int fret = startFret; fret <= endFret; fret++) {
				for (int schemeCounter = 0; schemeCounter < schemeSize; schemeCounter++) {
					int employedNoteIndex = buildInfo.employedNoteIndexes[schemeCounter];
					int openNoteFullIndex = buildInfo.openStringNoteFullIndexes[currentString];
					int currentNoteIndex = (openNoteFullIndex + fret) % FRETS_IN_OCTAVE;
					if (employedNoteIndex == currentNoteIndex) {
						boolean inSet = false;
						if (!buildInfo.usedNoteIndexSet.contains(currentNoteIndex)) {
							inSet = true;
							buildInfo.usedNoteIndexSet.add(currentNoteIndex);
						}
						buildInfo.frets[currentString] = fret;
						fillVariationSet(variationSet, buildInfo, currentString - 1, currentFret, startFret, endFret,
								Math.min(minFret, fret), Math.max(maxFret, fret));
						if (inSet) {
							buildInfo.usedNoteIndexSet.remove(currentNoteIndex);
							inSet = false;
						}
					}
				}
			}
		}
	}

	private static boolean validateBuildInfo(ChordBuildInfo buildInfo) {
		boolean result = true;
		int stringCount = buildInfo.openStringNoteFullIndexes.length;
		int minString = stringCount;
		int maxString = -1;
		for (int string = 0; string < stringCount; string++) {
			if (buildInfo.frets[string] >= 0) {
				minString = Math.min(minString, string);
				maxString = Math.max(maxString, string);
			}
		}
		result = ((maxString - minString) >= 2);
		if (result && buildInfo.chordType.hasOnlySinglePresenceOfEveryNote()) {
			Set<Integer> checkedNoteIndexes = new HashSet<Integer>();
			for (int string = 0; string < stringCount; string++) {
				int fret = buildInfo.frets[string];
				if (fret >= 0) {
					int noteIndex = (buildInfo.openStringNoteFullIndexes[string] + fret) % FRETS_IN_OCTAVE;
					if (checkedNoteIndexes.contains(noteIndex)) {
						result = false;
						break;
					} else {
						checkedNoteIndexes.add(noteIndex);
					}
				}
			}
		}
		if (result && (stringCount == 6)) {
			for (int[] exceptionScheme : sExceptionSchemes) {
				if (Arrays.equals(exceptionScheme, buildInfo.frets)) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	private static ChordVariation tryCreateChordVariation(ChordBuildInfo buildInfo, int currentString) {
		int stringCount = buildInfo.openStringNoteFullIndexes.length;
		int[] noteFullIndexes = new int[stringCount];
		Arrays.fill(noteFullIndexes, -1);
		for (int string = 0; string < stringCount; string++) {
			int fret = buildInfo.frets[string];
			if (fret >= 0) {
				int openNoteFullIndex = buildInfo.openStringNoteFullIndexes[string];
				int noteFullIndex = openNoteFullIndex + fret;
				noteFullIndexes[string] = noteFullIndex;
			}
		}
		int correctedBassNoteIndex = (buildInfo.chordType.hasNoBassNotes() ? 0 : buildInfo.bassNoteIndex);
		ChordVariation variation = ChordVariation.createVariationOrNull(buildInfo.chordType, buildInfo.rootNoteIndex,
				correctedBassNoteIndex, noteFullIndexes, buildInfo.frets);
		return variation;
	}

	private static class ChordBuildInfo {

		// блок данных, которые не меняются во время пробежки по циклу
		public final ChordType chordType;
		public final int rootNoteIndex;
		public final int[] openStringNoteFullIndexes;
		public final int[] employedNoteIndexes;
		public final Set<Integer> employedNoteIndexSet;

		// блок данных, которые меняются по ходу пробежки по циклу
		public int[] frets;
		public Set<Integer> usedNoteIndexSet;
		public int bassNoteIndex;

		public ChordBuildInfo(ChordType chordType, int rootNoteIndex, Tuning tuning) {
			this.chordType = chordType;
			this.rootNoteIndex = rootNoteIndex;

			int stringCount = tuning.getNotesCount();
			openStringNoteFullIndexes = new int[stringCount];
			for (int i = stringCount - 1; i >= 0; i--) {
				Note note = tuning.getNote(i);
				openStringNoteFullIndexes[stringCount - i - 1] = note.fullIndex;
			}

			int schemeSize = chordType.getSchemeSize();
			employedNoteIndexes = new int[schemeSize];
			employedNoteIndexSet = new HashSet<Integer>();
			for (int i = 0; i < schemeSize; i++) {
				int noteIndex = (rootNoteIndex + chordType.getSchemePositionForIndex(i)) % FRETS_IN_OCTAVE;
				employedNoteIndexes[i] = noteIndex;
				employedNoteIndexSet.add(noteIndex);
			}
		}
	}
}
