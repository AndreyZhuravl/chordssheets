package com.chordsheets.entities;

import java.util.ArrayList;
import java.util.List;

public class ChordSheet {

	private final List<ChordVariation> mChordVariationList;
	private final String mChordSheetName;

	public ChordSheet(List<ChordVariation> chordVariation, String chordSheetName) {
		mChordVariationList = new ArrayList<ChordVariation>(chordVariation);
		mChordSheetName = chordSheetName;
	}

	public List<ChordVariation> getChordVariationList() {
		return mChordVariationList;
	}

	public String getChordSheetName() {
		return mChordSheetName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mChordSheetName == null) ? 0 : mChordSheetName.hashCode());
		result = prime * result + ((mChordVariationList == null) ? 0 : mChordVariationList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChordSheet other = (ChordSheet) obj;
		if (mChordSheetName == null) {
			if (other.mChordSheetName != null)
				return false;
		} else if (!mChordSheetName.equals(other.mChordSheetName))
			return false;
		if (mChordVariationList == null) {
			if (other.mChordVariationList != null)
				return false;
		} else if (!mChordVariationList.equals(other.mChordVariationList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ChordSheet [mChordVariationList=");
		builder.append(mChordVariationList.toString());
		builder.append(", mChordSheetName=");
		builder.append(mChordSheetName.toString());
		builder.append("]");
		return builder.toString();
	}

}
