package com.chordsheets.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;

public final class ChordVariation implements Comparable<ChordVariation> {

	public static final int MAX_FINGERS = 4;
	public static final int MAX_BARRES = 2;
	private static final int EXCEPTION_BARRE_FRET = 2;
	private static final int[] EXCEPTION_BARRE_FRETS_ARRAY = new int[] { 2, 3, 2, 0, -1, -1 };

	public final ChordType chordType;

	public final int rootNoteIndex;
	public final int bassNoteIndex;
	private final int[] mNoteFullIndexes;
	private final int[] mFrets;
	private final List<Barre> mBarres;
	private final int[] mFingers;
	public final int fret, openStringsCount, playedStringsCount, usedFingers;

	/**
	 * @param chordType
	 *            - тип аккорда
	 * 
	 * @param rootNoteIndex
	 *            - индекс основной ноты
	 * @param bassNoteIndex
	 *            - индекс ноты альтернативного баса
	 * @param noteFullIndexes
	 *            - список полных индексов нот по струнам
	 * @param frets
	 *            - список ладов по струнам
	 * @param barres
	 *            - список баррэ
	 * @param fingers
	 *            - массив пальцев по струнам
	 */
	public ChordVariation(ChordType chordType, int rootNoteIndex, int bassNoteIndex, int[] noteFullIndexes,
			int[] frets, List<Barre> barres, int[] fingers) {
		this.chordType = chordType;
		this.rootNoteIndex = rootNoteIndex;
		this.bassNoteIndex = bassNoteIndex;
		mNoteFullIndexes = noteFullIndexes.clone();
		mFrets = frets.clone();
		mBarres = new ArrayList<Barre>(barres);
		mFingers = fingers.clone();
		openStringsCount = getOpenStringsCount(frets);
		playedStringsCount = getPlayedStringsCount(frets);
		usedFingers = getUsedFingers(fingers, mBarres.size());
		fret = getCorrectedMinimalFret(frets);
	}

	public int[] getNoteFullIndexes() {
		return mNoteFullIndexes;
	}

	public int[] getFrets() {
		return mFrets;
	}

	public List<Barre> getBarres() {
		return mBarres;
	}

	public int[] getFingers() {
		return mFingers;
	}

	public String getName(Context context, Note.NamingConvention noteNamingConvention) {
		StringBuilder sb = new StringBuilder();
		sb.append(noteNamingConvention.getNoteName(rootNoteIndex, false));
		sb.append(chordType.getShortName(context));
		if (rootNoteIndex != bassNoteIndex) {
			sb.append("/").append(noteNamingConvention.getNoteName(bassNoteIndex, false));
		}
		return sb.toString();
	}

	@Override
	public int compareTo(ChordVariation another) {
		int result = Integer.valueOf(fret).compareTo(another.fret);
		if (result == 0) {
			result = -Integer.valueOf(openStringsCount).compareTo(another.openStringsCount);
		}
		if (result == 0) {
			result = -Integer.valueOf(playedStringsCount).compareTo(another.playedStringsCount);
		}
		if (result == 0) {
			result = Integer.valueOf(usedFingers).compareTo(another.usedFingers);
		}
		if (result == 0) {
			result = Integer.valueOf(mFrets.length).compareTo(another.mFrets.length);
			if (result == 0) {
				int length = mFrets.length;
				for (int i = length - 1; i >= 0; i--) {
					if (result == 0) {
						result = Integer.valueOf(mFrets[i]).compareTo(another.mFrets[i]);
					}
				}
			}
		}
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fret;
		result = prime * result + openStringsCount;
		result = prime * result + playedStringsCount;
		result = prime * result + usedFingers;
		result = prime * result + Arrays.hashCode(mFrets);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ChordVariation other = (ChordVariation) obj;
		if (fret != other.fret) {
			return false;
		}
		if (openStringsCount != other.openStringsCount) {
			return false;
		}
		if (playedStringsCount != other.playedStringsCount) {
			return false;
		}
		if (usedFingers != other.usedFingers) {
			return false;
		}
		if (!Arrays.equals(mFrets, other.mFrets)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getSimpleName());
		builder.append(" [chordType=");
		builder.append(chordType);
		builder.append(", rootNoteIndex=");
		builder.append(rootNoteIndex);
		builder.append(", bassNoteIndex=");
		builder.append(bassNoteIndex);
		builder.append(", mNoteFullIndexes=");
		builder.append(Arrays.toString(mNoteFullIndexes));
		builder.append(", mFrets=");
		builder.append(Arrays.toString(mFrets));
		builder.append(", mBarres=");
		builder.append(mBarres);
		builder.append(", mFingers=");
		builder.append(Arrays.toString(mFingers));
		builder.append("]");
		return builder.toString();
	}

	/* Секция построения вариации */

	/**
	 * Фабричный метод для построения вариации. По контракту в массив frets не передаются совсем уж безумные комбинации
	 * вроде "1-я струна играет, 2-я не играет, 3 играет". Фильтр против такой комбинации должен проводиться на более
	 * высоком уровне.
	 * 
	 * @param chordType
	 *            - тип аккорда
	 * @param rootNoteIndex
	 *            - индекс основной ноты
	 * @param bassNoteIndex
	 *            - индекс ноты альтернативного баса
	 * @param noteFullIndexes
	 *            - массив нот по струнам
	 * @param frets
	 *            - массив ладов по струнам
	 * @return проинициализированную вариацию, если входные параметры корректны, или null
	 */
	public static ChordVariation createVariationOrNull(ChordType chordType, int rootNoteIndex, int bassNoteIndex,
			int[] noteFullIndexes, int[] frets) {
		ChordVariation variation = null;
		int minimalPositiveFret = getMinimalPositiveFret(frets);
		List<Barre> barres = new ArrayList<Barre>();
		int[] fingers = new int[frets.length];
		int fingerCount = 0;
		if (minimalPositiveFret != 0) {
			fingerCount = initFingersAndCapos(frets, minimalPositiveFret, barres, fingers);
		}
		/*
		 * на 3-м по счёту от minimalFret ладу не должны зажиматься струны выше баррэ
		 */
		if ((fingerCount <= MAX_FINGERS) && (barres.size() <= MAX_BARRES)) {
			if (isBarrePlacedCorrectly(frets, minimalPositiveFret + 3, barres)) {
				variation = new ChordVariation(chordType, rootNoteIndex, bassNoteIndex, noteFullIndexes, frets, barres,
						fingers);
			}
		}
		return variation;
	}

	/**
	 * Инициализирует массивы баррэ и пальцев.
	 * 
	 * @param frets
	 *            - массив ладов по струнам
	 * @param minimalPositiveFret
	 *            - минимальный положительный лад (равен нулю тогда и только тогда, когда нет ни одной зажатой струны)
	 * @param barres
	 *            - список баррэ
	 * @param fingers
	 *            - массив пальцев по струнам
	 * @return Количество пальцев по струнам.
	 */
	private static int initFingersAndCapos(int[] frets, int minimalPositiveFret, List<Barre> barres, int[] fingers) {
		final int size = fingers.length;
		int currentFinger = 0;
		for (int i = 0; i < size; i++) {
			fingers[i] = currentFinger;
		}
		/*
		 * Лад между минимальным и максимальным положительными ладами аккорда, на который не ставятся пальцы. В случае,
		 * если таких ладов 2, то берётся лад с наибольшим числом. Как можно видеть далее по коду, предыдущие свободные
		 * лады нет смысла запоминать.
		 */
		int freeFret = -1;
		for (int i = 0; i < Chord.MAX_FRETS_DISTANCE; i++) {
			int currentFret = minimalPositiveFret + i;
			int updatedFingerCount = buildFingersAndBarreForFret(frets, minimalPositiveFret, currentFret, barres,
					fingers, currentFinger);
			if (minimalPositiveFret > 0 && updatedFingerCount == currentFinger && i < Chord.MAX_FRETS_DISTANCE - 1) {
				freeFret = currentFret;
			}
			currentFinger = updatedFingerCount;
		}
		int freeFretCount = freeFret - minimalPositiveFret;
		while ((Chord.MAX_FRETS_DISTANCE - currentFinger > 0) && (freeFretCount > 0)) {
			for (Barre barre : barres) {
				if (barre.fret > freeFret) {
					barre.finger++;
					/*
					 * Этот финт ушами мы делаем, чтобы в следующем цикле по массиву fingers нам не пришлось корячиться
					 * и извлекать баррэ, чтобы ненароком не увеличить
					 */
					for (int i = barre.startString; i <= barre.lastString; i++) {
						if (frets[i] == barre.fret) {
							fingers[i]--;
						}
					}
				}
			}
			for (int i = 0; i < size; i++) {
				int fret = frets[i];
				if (fret > freeFret) {
					// смотрим внимательно: если эта струна зажималась баррэ, то
					// она в данный момент имеет отрицательный finger
					fingers[i]++;
					// теперь, если струна зажималась баррэ, она опять имеет
					// нулевой finger
				}
			}
			currentFinger++;
			freeFretCount--;
		}
		return currentFinger;
	}

	/**
	 * Строит на ладу currentFret пальцы и баррэ.
	 * 
	 * @param frets
	 *            - массив ладов по струнам
	 * @param minimalPositiveFret
	 *            - минимальный положительный лад (равен нулю тогда и только тогда, когда нет ни одной зажатой струны)
	 * @param currentFret
	 *            - текущий лад
	 * @param barres
	 *            - список баррэ
	 * @param fingers
	 *            - массив пальцев
	 * @param currentFinger
	 *            - текущий номер пальца
	 * @return - новый текущий номер пальца.
	 */
	private static int buildFingersAndBarreForFret(int[] frets, int minimalPositiveFret, int currentFret,
			List<Barre> barres, int[] fingers, int currentFinger) {
		int fingerCount = currentFinger;
		Barre barre = buildBarreForFret(frets, minimalPositiveFret, currentFret, currentFinger);
		int size = frets.length;
		/*
		 * stopString - струна, на которой можно завершать поиск одиночных пальцев на ладу. 1)Если capo окажется ==
		 * null, то завершаем на самой тонкой струне. 2)Если capo окажется != null, то поиск пальцев прекращается за
		 * одну струну от самой толстой струны баррэ.
		 */
		int stopString = 0;
		if (barre != null) {
			barres.add(barre);
			stopString = barre.lastString + 2;
			fingerCount++;
		}
		for (int i = size - 1; i >= stopString; i--) {
			int fret = frets[i];
			if (fret == currentFret) {
				fingerCount++;
				fingers[i] = fingerCount;
			}
		}
		return fingerCount;
	}

	/**
	 * Пытается построить баррэ на ладу == currentFret. Возвращает null, если не получилось построить.
	 * 
	 * @param frets
	 *            - разметка ладов по струнам
	 * @param minimalPositiveFret
	 *            - минимальный положительный лад (равен нулю тогда и только тогда, когда нет ни одной зажатой струны)
	 * @param currentFret
	 *            - текущий лад
	 * @param currentFinger
	 *            - текущий номер пальца
	 * @return Построенный баррэ или null.
	 */
	private static Barre buildBarreForFret(int[] frets, int minimalPositiveFret, int currentFret, int currentFinger) {
		if ((currentFret == EXCEPTION_BARRE_FRET) && isExceptionBarreFret(frets)) {
			return null;
		}
		int firstBarreString = -1;
		int lastBarreString = -1;
		int size = frets.length;
		/*
		 * В этом цикле мы ищем, с какой струны начинается баррэ. 1)Если струна не проигрывается или зажимается на ладу,
		 * большем currentFret, то мы продолжаем поиск. 2)Если струна зажимается на ладу, меньшему currentFret, то поиск
		 * завершается, firstCapoString остаётся == -1 3)Если струна зажимается на ладу, равном currentFret, то поиск с
		 * завершается с firstCapoString = i;
		 */
		for (int i = 0; i < size; i++) {
			int fret = frets[i];
			if (fret >= 0 && fret <= currentFret) {
				if (fret == currentFret) {
					firstBarreString = i;
				}
				break;
			}
		}
		if (firstBarreString >= 0) {
			/*
			 * В этом цикле мы ищем, на какой струне заканчивается баррэ. 1)Если струна зажимается на текущем ладу, то
			 * lastCapoString = i, но поиск продолжается. 2)Если струна зажимается на ладу, меньшем currentFret, поиск
			 * прекращается. 3)Если мы находимся не на минимальном положительном ладу, и струна зажимается на ладу,
			 * большем currentFret, lastCapoString = -1 и поиск прекращается.
			 */
			for (int i = firstBarreString + 1; i < size; i++) {
				int fret = frets[i];
				if (fret == currentFret) {
					lastBarreString = i;
				}
				if (fret < currentFret) {
					break;
				}
				if (fret > currentFret && currentFret > minimalPositiveFret) {
					lastBarreString = -1;
					break;
				}
			}
		}
		Barre capo = null;
		if (firstBarreString >= 0 && lastBarreString > firstBarreString) {
			capo = new Barre(currentFret, firstBarreString, lastBarreString, currentFinger + 1);
		}
		return capo;
	}

	private static final boolean isExceptionBarreFret(int[] frets) {
		boolean result = false;
		int length = frets.length;
		if (length == EXCEPTION_BARRE_FRETS_ARRAY.length) {
			result = true;
			for (int i = 0; i < length; i++) {
				if (frets[i] != EXCEPTION_BARRE_FRETS_ARRAY[i]) {
					result = false;
					break;
				}
			}
		}
		return result;
	}

	/*
	 * Секция примитивных показателей аккорда - минимальный лад, количество открытых струн и т.д.
	 */

	/**
	 * 
	 * @param frets
	 *            - разметка ладов
	 * @return Если хотя бы одна струна зажата, то минимальный ненулевой лад, 0 в противном случае (если струны или
	 *         открыты, или не проигрываются)
	 */
	private static final int getMinimalPositiveFret(int[] frets) {
		int minimum = Chord.FRETS_IN_OCTAVE + 1; // минимальный ненулевой лад.
		boolean allZeros = true;
		for (int fret : frets) {
			if (fret > 0) {
				allZeros = false;
				minimum = Math.min(minimum, fret);
			}
		}
		if (allZeros) {
			minimum = 0;
		}
		return minimum;
	}

	/**
	 * 
	 * @param frets
	 *            - разметка ладов
	 * @return 1, если хотя бы одна струна открыта, в противном случае - минимальный лад
	 */
	private static final int getCorrectedMinimalFret(int[] frets) {
		int minimum = Chord.FRETS_IN_OCTAVE + 1; // минимальный ненулевой лад.
		for (int fret : frets) {
			if (fret > 0) {
				minimum = Math.min(minimum, fret);
			} else if (fret == 0) {
				minimum = 1;
			}
		}
		return minimum;
	}

	/**
	 * 
	 * @param frets
	 *            - разметка ладов
	 * @return Количество открытых струн
	 */
	private static final int getOpenStringsCount(int[] frets) {
		int count = 0;
		for (int fret : frets) {
			if (fret == 0) {
				count++;
			}
		}
		return count;
	}

	/**
	 * 
	 * @param frets
	 *            - разметка ладов
	 * @return Количество играющих струн
	 */
	private static final int getPlayedStringsCount(int[] frets) {
		int count = 0;
		for (int fret : frets) {
			if (fret >= 0) {
				count++;
			}
		}
		return count;
	}

	/**
	 * 
	 * @param fingers
	 *            - массив пальцев по струнам
	 * @param capoCount
	 *            - количество баррэ в вариации
	 * @return Количество задействованных пальцев.
	 */
	private static int getUsedFingers(int[] fingers, int capoCount) {
		int count = capoCount;
		for (int finger : fingers) {
			if (finger > 0) {
				count++;
			}
		}
		return count;
	}

	/**
	 * 
	 * @param frets
	 *            - разметка ладов
	 * @param currentFret
	 *            - лад
	 * @param listCapos
	 *            - построенные баррэ
	 * @return Расположен ли корректно баррэ
	 */
	private static final boolean isBarrePlacedCorrectly(int[] frets, int currentFret, List<Barre> listCapos) {
		boolean flag = true;
		int size = listCapos.size();
		int stringCount = frets.length;
		for (int i = 0; i < size; i++) {
			Barre capo = listCapos.get(i);
			if (capo.fret == currentFret && capo.lastString < stringCount - 1) {
				for (int k = capo.lastString + 1; k < stringCount; k++) {
					if (frets[k] == currentFret) {
						flag = false;
					}
				}
			}
		}
		return flag;
	}
}
