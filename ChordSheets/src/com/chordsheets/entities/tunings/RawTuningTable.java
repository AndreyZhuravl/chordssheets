package com.chordsheets.entities.tunings;

import java.util.List;

import com.chordsheets.entities.Tuning;

/**
 * 
 * @author QuickNick
 * 
 */
public abstract class RawTuningTable {

	private final List<Tuning> mTuningTable;

	public RawTuningTable() {
		mTuningTable = initializeTuningTable();
	}

	protected abstract List<Tuning> initializeTuningTable();

	public List<Tuning> getTuningTable() {
		return mTuningTable;
	}
}
