package com.chordsheets.entities.tunings;

import java.util.ArrayList;
import java.util.List;

import com.chordsheets.R;
import com.chordsheets.entities.Tuning;

/**
 * Класс-утилита, возвращающая массив тюнингов 6-струнной гитары.
 * 
 * @author QuickNick
 * 
 */
public final class Guitar6StringTuningTable extends RawTuningTable {

	public Guitar6StringTuningTable() {
		super();
	}

	@Override
	protected List<Tuning> initializeTuningTable() {
		List<Tuning> resultList = new ArrayList<Tuning>();

		resultList.add(new Tuning(R.string.tuning_standard, new int[] { 4 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3, 7 + 12 * 3,
				11 + 12 * 3, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_all_Fourths, new int[] { 4 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3,
				7 + 12 * 3, 12 * 4, 5 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_baritone, new int[] { 11 + 12 * 1, 4 + 12 * 2, 9 + 12 * 2,
				2 + 12 * 3, 6 + 12 * 3, 11 + 12 * 3 }, false));
		resultList.add(new Tuning(R.string.tuning_C_sharp, new int[] { 1 + 12 * 2, 6 + 12 * 2, 11 + 12 * 2, 4 + 12 * 3,
				8 + 12 * 3, 1 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_C6, new int[] { 12 * 2, 9 + 12 * 2, 12 * 3, 7 + 12 * 3, 12 * 4,
				4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_crossnote_A, new int[] { 4 + 12 * 2, 9 + 12 * 2, 4 + 12 * 3,
				9 + 12 * 3, 12 * 4, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_crossnote_A2, new int[] { 4 + 12 * 2, 9 + 12 * 2, 12 * 3, 4 + 12 * 3,
				9 + 12 * 3, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_crossnote_D, new int[] { 2 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3,
				5 + 12 * 3, 9 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_crossnote_E, new int[] { 4 + 12 * 2, 11 + 12 * 2, 4 + 12 * 3,
				7 + 12 * 3, 11 + 12 * 3, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_crossnote_F, new int[] { 5 + 12 * 2, 8 + 12 * 2, 12 * 3, 5 + 12 * 3,
				12 * 4, 5 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_crossnote_F2, new int[] { 5 + 12 * 2, 12 * 3, 5 + 12 * 3, 9 + 12 * 3,
				12 * 4, 5 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_crossnote_G, new int[] { 2 + 12 * 2, 7 + 12 * 2, 2 + 12 * 3,
				7 + 12 * 3, 10 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_Csus2, new int[] { 12 * 2, 7 + 12 * 2, 12 * 3, 7 + 12 * 3, 12 * 4,
				2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_D_modal, new int[] { 2 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3, 7 + 12 * 3,
				9 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_D_modal2, new int[] { 2 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3, 9 + 12 * 3,
				2 + 12 * 4, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_double_Drop_A, new int[] { 9 + 12 * 1, 4 + 12 * 2, 9 + 12 * 2,
				2 + 12 * 3, 6 + 12 * 3, 9 + 12 * 3 }, false));
		resultList.add(new Tuning(R.string.tuning_double_Drop_B_flat, new int[] { 10 + 12 * 1, 5 + 12 * 2, 10 + 12 * 2,
				3 + 12 * 3, 7 + 12 * 3, 10 + 12 * 3 }, false));
		resultList.add(new Tuning(R.string.tuning_double_Drop_B, new int[] { 11 + 12 * 1, 6 + 12 * 2, 11 + 12 * 2,
				4 + 12 * 3, 8 + 12 * 3, 11 + 12 * 3 }, false));
		resultList.add(new Tuning(R.string.tuning_double_Drop_C, new int[] { 12 * 2, 7 + 12 * 2, 12 * 3, 5 + 12 * 3,
				9 + 12 * 3, 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_double_Drop_C_sharp, new int[] { 1 + 12 * 2, 8 + 12 * 2, 1 + 12 * 3,
				6 + 12 * 3, 10 + 12 * 3, 1 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_double_Drop_D, new int[] { 2 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3,
				7 + 12 * 3, 11 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_drop_A, new int[] { 9 + 12 * 1, 4 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3,
				6 + 12 * 3, 11 + 12 * 3 }, false));
		resultList.add(new Tuning(R.string.tuning_drop_A2, new int[] { 9 + 12 * 1, 9 + 12 * 2, 2 + 12 * 3, 7 + 12 * 3,
				11 + 12 * 3, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_drop_B_flat, new int[] { 10 + 12 * 1, 5 + 12 * 2, 10 + 12 * 2,
				3 + 12 * 3, 7 + 12 * 3, 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_drop_B, new int[] { 11 + 12 * 1, 6 + 12 * 2, 11 + 12 * 2, 4 + 12 * 3,
				8 + 12 * 3, 1 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_drop_C, new int[] { 12 * 2, 7 + 12 * 2, 12 * 3, 5 + 12 * 3,
				9 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_drop_C_sharp, new int[] { 1 + 12 * 2, 8 + 12 * 2, 1 + 12 * 3,
				6 + 12 * 3, 10 + 12 * 3, 3 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_drop_D, new int[] { 2 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3, 7 + 12 * 3,
				11 + 12 * 3, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_Dsus2, new int[] { 2 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3, 4 + 12 * 3,
				9 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_E_modal, new int[] { 4 + 12 * 2, 11 + 12 * 2, 4 + 12 * 3, 4 + 12 * 3,
				11 + 12 * 3, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_Esus2, new int[] { 4 + 12 * 2, 9 + 12 * 2, 4 + 12 * 3, 6 + 12 * 3,
				11 + 12 * 3, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_Esus4, new int[] { 4 + 12 * 2, 9 + 12 * 2, 4 + 12 * 3, 9 + 12 * 3,
				11 + 12 * 3, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_Eb_Hendrix, new int[] { 3 + 12 * 2, 8 + 12 * 2, 1 + 12 * 3,
				6 + 12 * 3, 10 + 12 * 3, 3 + 12 * 4 }, true));
		resultList.add(new Tuning(R.string.tuning_full_Step_Down, new int[] { 2 + 12 * 2, 7 + 12 * 2, 12 * 3,
				5 + 12 * 3, 9 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_G_modal, new int[] { 7 + 12 * 1, 7 + 12 * 2, 2 + 12 * 3, 7 + 12 * 3,
				11 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_gambale, new int[] { 9 + 12 * 1, 2 + 12 * 2, 7 + 12 * 2, 12 * 3,
				4 + 12 * 3, 9 + 12 * 3 }, false));
		resultList.add(new Tuning(R.string.tuning_Gsus2, new int[] { 2 + 12 * 2, 7 + 12 * 2, 2 + 12 * 3, 7 + 12 * 3,
				9 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_Gsus4, new int[] { 2 + 12 * 2, 7 + 12 * 2, 2 + 12 * 3, 7 + 12 * 3,
				12 * 4, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_Gsus4_2, new int[] { 2 + 12 * 2, 7 + 12 * 2, 12 * 3, 7 + 12 * 3,
				12 * 4, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_guinnevere, new int[] { 4 + 12 * 2, 11 + 12 * 2, 2 + 12 * 3,
				7 + 12 * 3, 9 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_new_Standard, new int[] { 12 * 2, 7 + 12 * 2, 2 + 12 * 3, 9 + 12 * 3,
				4 + 12 * 4, 7 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_A, new int[] { 4 + 12 * 2, 9 + 12 * 2, 1 + 12 * 3, 4 + 12 * 3,
				9 + 12 * 3, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_A2, new int[] { 4 + 12 * 2, 9 + 12 * 2, 1 + 12 * 3, 4 + 12 * 3,
				9 + 12 * 3, 1 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_A_slide, new int[] { 4 + 12 * 2, 9 + 12 * 2, 4 + 12 * 3,
				9 + 12 * 3, 1 + 12 * 4, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_C, new int[] { 12 * 2, 7 + 12 * 2, 12 * 3, 7 + 12 * 3, 12 * 4,
				4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_C_sharp, new int[] { 1 + 12 * 2, 8 + 12 * 2, 1 + 12 * 3,
				5 + 12 * 3, 8 + 12 * 3, 1 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_D, new int[] { 2 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3, 6 + 12 * 3,
				9 + 12 * 3, 2 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_E_flat, new int[] { 3 + 12 * 2, 10 + 12 * 2, 3 + 12 * 3,
				7 + 12 * 3, 10 + 12 * 3, 3 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_E, new int[] { 4 + 12 * 2, 11 + 12 * 2, 4 + 12 * 3, 8 + 12 * 3,
				11 + 12 * 3, 4 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_F, new int[] { 5 + 12 * 2, 9 + 12 * 2, 12 * 3, 5 + 12 * 3,
				12 * 4, 5 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_F2, new int[] { 12 * 2, 5 + 12 * 2, 12 * 3, 5 + 12 * 3,
				9 + 12 * 3, 5 + 12 * 4 }, false));
		resultList.add(new Tuning(R.string.tuning_open_G, new int[] { 2 + 12 * 2, 7 + 12 * 2, 2 + 12 * 3, 7 + 12 * 3,
				11 + 12 * 3, 2 + 12 * 4 }, false));

		return resultList;
	}
}
