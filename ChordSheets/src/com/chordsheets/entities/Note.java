package com.chordsheets.entities;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Класс-хранитель информации о ноте: октава, индекс, полный индекс, частота
 * 
 * @author <a href="mailto:niko89a@yandex.ru">QuickNick</a>
 */
public final class Note implements Comparable<Note> {
	
	public static final int MAX_OCTAVE = 8;
	public static final int TONES_IN_OCTAVE = 12;

	/**
	 * Номер октавы ноты
	 */
	public final int octave;
	/**
	 * Индекс ноты
	 */
	public final int index;
	/**
	 * Полный индекс ноты: octave*tones+index
	 */
	public final int fullIndex;
	/**
	 * Частота ноты
	 */
	public final float frequency;

	public Note(int octave, int index) {
		this(octave, index, 0);
	}

	public Note(int octave, int index, float frequency) {
		this.octave = octave;
		this.index = index;
		fullIndex = octave * TONES_IN_OCTAVE + index;
		this.frequency = frequency;
	}

	@Override
	public int compareTo(Note another) {
		int result = Integer.valueOf(fullIndex).compareTo(another.fullIndex);
		if (result == 0) {
			result = Float.compare(frequency, another.frequency);
		}
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Note)) {
			return false;
		}
		Note rhs = (Note) o;
		return compareTo(rhs) == 0;
	}

	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + fullIndex;
		result = 31 * result + Float.floatToIntBits(frequency);
		return result;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getSimpleName());
		builder.append(" [fullIndex=");
		builder.append(fullIndex);
		builder.append(", frequency=");
		builder.append(frequency);
		builder.append("]");
		return builder.toString();
	}

	public static final class NamingConvention {

		public static final NamingConvention ENGLISH_NAMING_CONVENTION_WITH_SPECIAL_SYMBOLS = new NamingConvention(
				//
				new String[] { "C", "C\u266F", "D", "D\u266F", "E", "F", "F\u266F", "G", "G\u266F", "A", "A\u266F", "B" }, //
				new String[] { "C", "D\u266D", "D", "E\u266D", "E", "F", "G\u266D", "G", "A\u266D", "A", "B\u266D", "B" });
		public static final NamingConvention ENGLISH_NAMING_CONVENTION = new NamingConvention( //
				new String[] { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" }, //
				new String[] { "C", "Db", "D", "Eb", "E", "F", "Gb", "G", "Ab", "A", "Bb", "B" });;
		private final List<String> mNamesWithSharps;
		private final List<String> mNamesWithFlats;

		public NamingConvention(String[] namesWithSharps, String[] namesWithFlats) {
			super();
			mNamesWithSharps = Collections.unmodifiableList(Arrays.asList(namesWithSharps));
			mNamesWithFlats = Collections.unmodifiableList(Arrays.asList(namesWithFlats));
		}

		public List<String> getNoteNameList(boolean supposeFlats) {
			List<String> resultList = (supposeFlats ? mNamesWithFlats : mNamesWithSharps);
			return resultList;
		}

		public String getNoteName(int noteIndex, boolean supposeFlats) {
			return getNoteNameList(supposeFlats).get(noteIndex);
		}

		public String getNoteName(Note note, boolean supposeFlats) {
			return getNoteName(note.index, supposeFlats);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((mNamesWithSharps == null) ? 0 : mNamesWithSharps.hashCode());
			result = prime * result + ((mNamesWithFlats == null) ? 0 : mNamesWithFlats.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			NamingConvention other = (NamingConvention) obj;
			if (mNamesWithSharps == null) {
				if (other.mNamesWithSharps != null) {
					return false;
				}
			} else if (!mNamesWithSharps.equals(other.mNamesWithSharps)) {
				return false;
			}
			if (mNamesWithFlats == null) {
				if (other.mNamesWithFlats != null) {
					return false;
				}
			} else if (!mNamesWithFlats.equals(other.mNamesWithFlats)) {
				return false;
			}
			return true;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append(getClass().getSimpleName());
			builder.append(" [mNamesWithSharps=");
			builder.append(mNamesWithSharps);
			builder.append(", mNamesWithFlats=");
			builder.append(mNamesWithFlats);
			builder.append("]");
			return builder.toString();
		}

	}

}
