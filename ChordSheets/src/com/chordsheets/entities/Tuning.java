package com.chordsheets.entities;

import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;

/**
 * Класс, описывающий тюнинг струнного инструмента.
 * 
 * @author QuickNick
 * 
 */
public final class Tuning {

	/**
	 * Указатель на название тюнинга
	 */
	private final int mNameRes;
	/**
	 * Список нот, которые звучат при открытых струнах, начиная с наименьшей частоты.
	 * 
	 * Рассмотрим стандартный тюнинг. Его образующие ноты: E2-A2-D3-G3-B3-E4.
	 * <ol>
	 * <li>E2 - нота Ми большой октавы (в каждой октаве 12 нот, считая диезные). Нота Ми - 4-я по счёту, если как
	 * программисты, считать с нуля (на нулевой позивии До). Следовательно, полный номер E2 - это 12 * 2 + 4 = 28.</li>
	 * <li>A2 - нотя Ля большой октавы. Нота Ля - 9-я по счёту. Полный номер A2 - это 12 * 2 + 9 = 33.</li>
	 * <li>И т.д.</li>
	 * </ol>
	 * Таким образом, стандартный тюнинг описывается следующим массивом:<br />
	 * { 4 + 12 * 2, 9 + 12 * 2, 2 + 12 * 3, 7 + 12 * 3, 11 + 12 * 3, 4 + 12 * 4 }<br />
	 * От себя очень рекомендую записывать номер ноты в массиве по формату 12n+m (или m+12n), чтобы тюнинг выглядел
	 * прозрачней. Обфускатор потом всё равно оптимизирует, а код читать нам.
	 * 
	 * @author QuickNick
	 */
	private final List<Note> mNotesList;

	private boolean mSupposeNoteNamesWithFlats;

	/**
	 * Конструктор, принимающий на вход название тюнинга и список образующих нот.
	 * 
	 * @param name
	 *            - название тюнинга
	 * @param notes
	 *            - список образующих нот.
	 */
	public Tuning(int nameRes, int[] notes, boolean supposeNoteNamesWithFlats) {
		mNameRes = nameRes;
		mSupposeNoteNamesWithFlats = supposeNoteNamesWithFlats;
		int size = notes.length;
		Note[] noteArray = new Note[size];
		for (int i = 0; i < size; i++) {
			noteArray[i] = new Note(notes[i] / 12, notes[i] % 12);
		}
		mNotesList = Arrays.asList(noteArray);
	}

	/**
	 * 
	 * @return название тюнинга
	 */
	public String getName(Context context) {
		String result = context.getString(mNameRes);
		return result;
	}

	public String getName(Resources resources) {
		String result = resources.getString(mNameRes);
		return result;
	}

	public int getNotesCount() {
		return mNotesList.size();
	}

	public Note getNote(int position) {
		return mNotesList.get(position);
	}

	public int getNoteIndex(Note note) {
		int index = -1;
		int size = mNotesList.size();
		for (int i = 0; i < size; i++) {
			Note tuningNote = mNotesList.get(i);
			if (tuningNote.fullIndex == note.fullIndex) {
				index = i;
				break;
			}
		}
		return index;
	}

	public boolean isSupposeNoteNamesWithFlats() {
		return mSupposeNoteNamesWithFlats;
	}

	/**
	 * Конструирует строку - альтернативное название тюнинга по его образующим нотам. Например, стандартный тюнинг могут
	 * называть: EADGBE.
	 * 
	 * @param namingConvention
	 *            - названия нот по тому или иному стандарту:
	 *            <ul>
	 *            <li>Англосаксонский стандарт с диезами: C-C#-D-D#-E-F-F#-G-G#-A-A#-B или бемолями
	 *            C-Db-D-Eb-E-F-Gb-G-Ab-A-Bb-B</li>
	 *            <li>Центральноевропейский стандарт с диезами: C-C#-D-D#-E-F-F#-G-G#-A-A#-H или бемолями:
	 *            C-Db-D-Eb-E-F-Gb-G-Ab-A-Hb-H</li>
	 *            <ul>
	 * @return Название тюнинга по его образующим нотам.
	 */
	public String getNameByNotes(Note.NamingConvention namingConvention) {
		List<String> names = namingConvention.getNoteNameList(mSupposeNoteNamesWithFlats);
		StringBuilder resultBuilder = new StringBuilder();
		int size = mNotesList.size();
		for (int i = 0; i < size; i++) {
			int noteIndex = mNotesList.get(i).index;
			String noteName = names.get(noteIndex);
			resultBuilder.append(noteName);
		}
		return resultBuilder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + mNotesList.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Tuning other = (Tuning) obj;
		boolean result = mNotesList.equals(other.mNotesList);
		return result;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getSimpleName());
		builder.append(" [mNameRes=");
		builder.append(mNameRes);
		builder.append(", mNotesList=");
		builder.append(mNotesList);
		builder.append(", mSupposeNoteNamesWithFlats=");
		builder.append(mSupposeNoteNamesWithFlats);
		builder.append("]");
		return builder.toString();
	}

}
