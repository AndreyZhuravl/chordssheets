package com.chordsheets.entities;

/**
 * Класс-хранитель данных о баррэ: на каком ладу берётся, с какой струны начинается и заканчивается, каким пальцем
 * зажимается
 * 
 * @author Irina, QuickNick
 * 
 */
public final class Barre {

	/**
	 * Лад, на котором берётся баррэ
	 */
	public final int fret;
	/**
	 * Струна, с которой начинается баррэ. По контракту меньше lastString
	 */
	public final int startString;
	/**
	 * Струна, на которой заканчивается баррэ
	 */
	public final int lastString;
	/**
	 * Номер пальца, которым берётся баррэ
	 */
	public int finger;

	public Barre(int fret, int startString, int lastString, int finger) {
		this.fret = fret;
		this.startString = startString;
		this.lastString = lastString;
		this.finger = finger;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fret;
		result = prime * result + startString;
		result = prime * result + lastString;
		result = prime * result + finger;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Barre other = (Barre) obj;
		if (fret != other.fret) {
			return false;
		}
		if (startString != other.startString) {
			return false;
		}
		if (lastString != other.lastString) {
			return false;
		}
		if (finger != other.finger) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getSimpleName());
		builder.append(" [fret=");
		builder.append(fret);
		builder.append(", startString=");
		builder.append(startString);
		builder.append(", lastString=");
		builder.append(lastString);
		builder.append(", finger=");
		builder.append(finger);
		builder.append("]");
		return builder.toString();
	}


}
