package com.chordsheets.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;

import com.chordsheets.R;

public final class ChordType {

	private static final int MASK_FIFTH_ABSENCE_POSSIBLE = 1 << 0;
	private static final int MASK_THINNEST_STRING_ABSENCE_POSSIBLE = 1 << 1;
	private static final int MASK_NO_BASS_NOTES = 1 << 2;
	private static final int MASK_ONLY_SINGLE_PRESENCE_OF_EVERY_NOTE = 1 << 3;

	private final int mNameRes;
	private final int mShortNameRes;
	private final int[] mScheme;
	public final int propertiesMask;

	public ChordType(int nameRes, int shortNameRes, int[] scheme, int propertiesMask) {
		super();
		mNameRes = nameRes;
		mShortNameRes = shortNameRes;
		mScheme = scheme.clone();
		this.propertiesMask = propertiesMask;
	}

	public ChordType(int nameRes, int[] scheme, int propertiesMask) {
		this(nameRes, nameRes, scheme, propertiesMask);
	}

	public ChordType(int nameRes, int shortNameRes, int[] scheme) {
		this(nameRes, shortNameRes, scheme, 0);
	}

	public ChordType(int nameRes, int[] scheme) {
		this(nameRes, nameRes, scheme);
	}

	public String getName(Resources res) {
		return res.getString(mNameRes);
	}

	public String getName(Context context) {
		return getName(context.getResources());
	}

	public String getShortName(Resources res) {
		return res.getString(mShortNameRes);
	}

	public String getShortName(Context context) {
		return getShortName(context.getResources());
	}

	public int getSchemeSize() {
		return mScheme.length;
	}

	public int getSchemePositionForIndex(int index) {
		return mScheme[index];
	}

	public boolean isFifthAbsencePossible() {
		return ((propertiesMask & MASK_FIFTH_ABSENCE_POSSIBLE) == MASK_FIFTH_ABSENCE_POSSIBLE);
	}

	public boolean isThinnestStringAbsencePossible() {
		return ((propertiesMask & MASK_THINNEST_STRING_ABSENCE_POSSIBLE) == MASK_THINNEST_STRING_ABSENCE_POSSIBLE);
	}

	public boolean hasNoBassNotes() {
		return ((propertiesMask & MASK_NO_BASS_NOTES) == MASK_NO_BASS_NOTES);
	}

	public boolean hasOnlySinglePresenceOfEveryNote() {
		return ((propertiesMask & MASK_ONLY_SINGLE_PRESENCE_OF_EVERY_NOTE) == MASK_ONLY_SINGLE_PRESENCE_OF_EVERY_NOTE);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(mScheme);
		result = prime * result + propertiesMask;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ChordType other = (ChordType) obj;
		if (!Arrays.equals(mScheme, other.mScheme)) {
			return false;
		}
		if (propertiesMask != other.propertiesMask) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getSimpleName());
		builder.append(" [mScheme=");
		builder.append(Arrays.toString(mScheme));
		builder.append("]");
		return builder.toString();
	}

	public static final List<ChordType> getAvailableChordTypes() {
		int firstStringAbsencePossible = MASK_THINNEST_STRING_ABSENCE_POSSIBLE;
		int noFifthPossible = MASK_FIFTH_ABSENCE_POSSIBLE;
		int dim7ChordPropertyMask = MASK_NO_BASS_NOTES | MASK_THINNEST_STRING_ABSENCE_POSSIBLE
				| MASK_ONLY_SINGLE_PRESENCE_OF_EVERY_NOTE;
		List<ChordType> resultList = new ArrayList<ChordType>();

		resultList
				.add(new ChordType(R.string.chord_type_major, R.string.chord_type_major_short, new int[] { 0, 4, 7 }));
		resultList
				.add(new ChordType(R.string.chord_type_minor, R.string.chord_type_minor_short, new int[] { 0, 3, 7 }));
		resultList.add(new ChordType(R.string.chord_type_dim, new int[] { 0, 3, 6 }, firstStringAbsencePossible));
		resultList.add(new ChordType(R.string.chord_type_aug, new int[] { 0, 4, 8 }));
		resultList.add(new ChordType(R.string.chord_type_2, new int[] { 0, 2, 4, 7 }));

		resultList.add(new ChordType(R.string.chord_type_7, new int[] { 0, 4, 7, 10 }));
		resultList.add(new ChordType(R.string.chord_type_m7, new int[] { 0, 3, 7, 10 }));
		resultList.add(new ChordType(R.string.chord_type_maj7, new int[] { 0, 4, 7, 11 }));
		resultList.add(new ChordType(R.string.chord_type_dim7, new int[] { 0, 3, 6, 9 }, dim7ChordPropertyMask));
		resultList.add(new ChordType(R.string.chord_type_m_maj7, new int[] { 0, 3, 7, 11 }));

		resultList.add(new ChordType(R.string.chord_type_7plus5, new int[] { 0, 4, 8, 10 }));
		resultList.add(new ChordType(R.string.chord_type_7sus2, new int[] { 0, 2, 7, 10 }));
		resultList.add(new ChordType(R.string.chord_type_7sus4, new int[] { 0, 5, 7, 10 }));
		resultList.add(new ChordType(R.string.chord_type_6, new int[] { 0, 4, 7, 9 }));
		resultList.add(new ChordType(R.string.chord_type_m6, new int[] { 0, 3, 7, 9 }));

		resultList.add(new ChordType(R.string.chord_type_9, new int[] { 0, 4, 7, 10, 2 }, noFifthPossible));
		resultList.add(new ChordType(R.string.chord_type_minus9, new int[] { 0, 4, 7, 10, 1 }, noFifthPossible));
		resultList.add(new ChordType(R.string.chord_type_m9, new int[] { 0, 3, 7, 10, 2 }, noFifthPossible));
		resultList.add(new ChordType(R.string.chord_type_m_minus9, new int[] { 0, 3, 7, 10, 1 }, noFifthPossible));
		resultList.add(new ChordType(R.string.chord_type_maj9, new int[] { 0, 4, 7, 11, 2 }, noFifthPossible));

		resultList.add(new ChordType(R.string.chord_type_sus2, new int[] { 0, 2, 7 }));
		resultList.add(new ChordType(R.string.chord_type_sus4, new int[] { 0, 5, 7 }));
		resultList.add(new ChordType(R.string.chord_type_add9, new int[] { 0, 4, 7, 2 }));
		resultList.add(new ChordType(R.string.chord_type_5, new int[] { 0, 7 }, firstStringAbsencePossible));

		return resultList;
	}

}
