package com.chordsheets.entities;

import java.util.ArrayList;
import java.util.List;

public final class NoteSystem {

	private final List<Note> mListNotes;

	public NoteSystem(List<Note> listNotes) {
		mListNotes = new ArrayList<Note>(listNotes);
	}

	public List<Note> getListNotes() {
		return mListNotes;
	}

	public List<Note> cutFragmentListNotes(Note from, Note to) {
		List<Note> list = mListNotes.subList(from.fullIndex, to.fullIndex + 1);
		return list;
	}

	public List<Note> cutNoteByIndex(int index) {
		List<Note> list = new ArrayList<Note>();
		for (int i = 0; i < Note.MAX_OCTAVE; i++) {
			list.add(mListNotes.get(i * Note.TONES_IN_OCTAVE + index));
		}
		return list;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((mListNotes == null) ? 0 : mListNotes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NoteSystem other = (NoteSystem) obj;
		if (mListNotes == null) {
			if (other.mListNotes != null) {
				return false;
			}
		} else if (!mListNotes.equals(other.mListNotes)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(getClass().getSimpleName());
		builder.append(" [mListNotes=");
		builder.append(mListNotes);
		builder.append("]");
		return builder.toString();
	}
}
