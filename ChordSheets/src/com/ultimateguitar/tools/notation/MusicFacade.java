package com.ultimateguitar.tools.notation;

import com.chordsheets.entities.Note;
import com.chordsheets.entities.NoteSystem;
import com.chordsheets.entities.temperament.EqualTemperament12;
import com.chordsheets.entities.temperament.Temperament.TemperamentTypes;

/**
 * @author <a href="mailto:niko89a@yandex.ru">QuickNick</a>
 */
public final class MusicFacade {

	private static final float sLog2 = (float) Math.log(2);
	private static final EqualTemperament12 sEqualTemperament12;
	public static final Note BASENOTE;
	public static final NoteSystem ETALON_NOTES;

	static {
		sEqualTemperament12 = new EqualTemperament12();
		BASENOTE = new Note(4, 9, 440.0f);
		ETALON_NOTES = sEqualTemperament12.generateNoteSystem(BASENOTE);
	}

	/**
	 * 
	 * @param freq1
	 *            - 1-я частота
	 * @param freq2
	 *            - 2-я частота
	 * @param digitsAfterDot
	 *            - количество знаков после запятой, которые нужно оставить у частот перед вычислением количества центов
	 * @return количество центов на интервале [freq1, freq2]
	 */
	public static float centDifference(float freq1, float freq2, int digitsAfterDot) {
		float multiplyer = digitsAfterDot * 10;
		float adjustedFreq1 = Math.round(freq1 * multiplyer) / multiplyer;
		float adjustedFreq2 = Math.round(freq2 * multiplyer) / multiplyer;
		return (float) (1200 * Math.log(adjustedFreq2 / adjustedFreq1) / sLog2);
	}

	public static NoteSystem generateNoteSystem(TemperamentTypes type, Note baseNote) {
		NoteSystem noteSystem = null;
		switch (type) {
			case EQUAL_12: {
				noteSystem = sEqualTemperament12.generateNoteSystem(baseNote);
				break;
			}
			default: { //
			}
		}
		return noteSystem;
	}
}
